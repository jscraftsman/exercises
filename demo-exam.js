function solution(A) {
	const arraySize = A.length
	const sortedArray = A.sort()

	let indexWithGap = 0
	let smallest = 1
	for (let i in sortedArray) {
		if ((+i + 1) >= arraySize) break
		if (sortedArray[i] < 0) {
			indexWithGap++
			continue
		}

		let gap = sortedArray[+i + 1] - sortedArray[i]

		if (gap !== 1) {
			if (gap === 0) {
				indexWithGap++
				continue
			}
			break
		} else {
			indexWithGap++
		}

	}

	smallest = sortedArray[indexWithGap] + 1
	if (smallest < 1) {
		smallest = 1
	}

	return smallest
}

// TODO
// - [DONE] Skip to 0 if there negative itesm
// - [DONE] Consider duplicate items
// - [DONE] Negative with atleast '1' in the array

let result = null
let expected = null
let actual = null
let input = null

input = [1, 3]
expected = 2
actual = solution(input)
result = actual === expected
console.log('[1] solution', result, expected, actual, input)

input = [1, 2]
expected = 3
actual = solution(input)
result = actual === expected
console.log('[2] solution', result, expected, actual, input)

input = [-1, -3]
expected = 1
actual = solution(input)
result = actual === expected
console.log('[3] solution', result, expected, actual, input)

input = [2, 1, 1]
expected = 3
actual = solution(input)
result = actual === expected
console.log('[4] solution', result, expected, actual, input)

input = [-1, -2, 1]
expected = 2
actual = solution(input)
result = actual === expected
console.log('[4] solution', result, expected, actual, input)
