// TEST CASES

let inputs = null
let expected = null
let actual = null

testOneSuite()
testTwoSuite()
testThreeSuite()

// [Debug] Validation
// validateAllUniqueFunction()

/////////////////////////////////////////////////////

// SOLUTIONS

function one(input) {
	let smallest = input[0]

	for (let item of input) {
		if (item < smallest) {
			smallest = item
		}
	}
	return smallest
}

function two(n) {
	let output = '+'
	let i = 1
	while (i < n) {
		output += (i % 2 == 0 ? '+' : '-')
		i++
	}
	return output
}

function three(n) {
	const half = Math.floor(n / 2)
	const items = []
	let i = 0
	let current = 0

	if (n % 2 !== 0) {
		current = 0
		items.push(current)
	}

	current = 1
	while (i < half) {
		if (items.indexOf(current) === -1 && items.indexOf(-current) === -1) {
			items.unshift(current)
			items.push(current * -1)
			current++
		}
		i++
	}

	return items
}

/////////////////////////////////////////////////////

// TEST SUITES

function testOneSuite() {
	console.log('\n[INFO] Test Cases for One')

	const specs = [
		{ expected: 1,		input: [5, 4, 3, 2, 1] },
		{ expected: 0,		input: [5, 0, 3, 2, 1] },
		{ expected: -2,		input: [-1, 1, 2, -2] },
		{ expected: -999,	input: [-999, -100, -1, 0, 1, 100, 999, 1000] },
		{ expected: -1000,	input: [-1000, -999, -100, -1, 0, 1, 100, 999, 1000] },
	]

	let testCaseNumber = 1

	for (let spec of specs) {
		input = spec.input 
		expected = spec.expected 
		actual = one(input)

		testSpec(1, testCaseNumber++, input, expected, actual)
	}
}

function testTwoSuite() {
	console.log('\n[INFO] Test Cases for Two')

	const specs = [
		{ input: 1, expected: '+' },
		{ input: 4, expected: '+-+-' },
		{ input: 5, expected: '+-+-+' },
		{ input: 50, expected: '+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-' },
		{ input: 99, expected: '+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+' },
		{ input: 100, expected: '+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-' },
		{ input: 101, expected: '+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+' },
	]

	let testCaseNumber = 1

	for (let spec of specs) {
		input = spec.input 
		expected = spec.expected 
		actual = two(input)

		testSpec(2, testCaseNumber++, input, expected, actual)
	}
}

function testThreeSuite() {
	console.log('\n[INFO] Test Cases for Three')

	let pass = false

	const specs = [
		{ input: 1, expected: [0] },
		{ input: 2, expected: [1, -1] },
		{ input: 3, expected: [1, 0, -1] },
		{ input: 4, expected: [2, 1, -1, -2] },
		{ input: 5, expected: [2, 1, 0, -1, -2] }
	]

	let testCaseNumber = 1

	for (let spec of specs) {
		input = spec.input 
		expected = spec.expected 
		actual =  three(input)
		pass = allUnique(actual) && sumIsZero(actual) && actual.length === input && isEqualArray(expected, actual)

		testSpec(3, testCaseNumber++, input, expected, actual, pass)
	}
}

// HELPER FUNCTIONS
function testSpec(n, i, input, expected, actual, _pass) {
	let pass = _pass ? _pass : expected === actual
	// let log = `[${n}-${i}]\tPass: ${pass}\n\tInput: ${input}\n\tExpected: ${expected}\n\tActual:   ${actual}\n`
	let log = `[${n}-${i}]\tPass: ${pass}`
	console.log(log)
}

function validateAllUniqueFunction() {
	console.log('\n[INFO] Validation for allunique() function')

	console.log('[Debug] allUnique:', allUnique([1, 0, -3, 2]))
	console.log('[Debug] allUnique:', allUnique([-2, 1, -4, 5]))
	console.log('[Debug] allUnique:', allUnique([1, -1, 1, 3]))
	console.log('[Debug] allUnique:', allUnique([-1, 0, 1]))
}

function allUnique(arr) {
	let allUnique = true 
	let items = []

	for (let i of arr) {
		if (items.indexOf(i) !== -1) {
			allUnique = false
			break
		}
		items.push(i)
	}

	// console.log('[Debug] allUnique', allUnique)
	return allUnique
}

function sumIsZero(arr) {
	let total = 0
	for (let i of arr) {
		total += i
	}

	// console.log('[Debug] Total', total)
	return total === 0
}

function isEqualArray(a, b) {
	if (a.length !== b.length) return false

	let x = a.sort()
	let y = b.sort()
	for (let i in x) {
		if (x[i] !== y[i]) {
			return false
		}
	}
	
	// console.log('[Debug]\na', a, '\nb', b)
	return true
}
